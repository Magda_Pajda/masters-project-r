# README #

### What is this repository for? ###

* In order to present one part of my master's project. Topic of my master thesis: Detecting change-point in linear regression. In the theoretical part I used a theorem from Horvath (Detecting changes in linear Regressions). I wrote my simulation in order to check if this theorem is useful in practice.  

### How do I get set up? ###
* RSTudio program or use Amazon Web Service http://www.louisaslett.com/RStudio_AMI/
* Create folders: Function, results
* save a file function_model_III.R in Function folder
* results folder will be used for saving result

